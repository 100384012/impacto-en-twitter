package cdist;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.nio.channels.ServerSocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Scanner;

import com.vdurmont.emoji.EmojiParser;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterProducer {

	public static final String _consumerKey = "4uDKAPtVbEcVnTJyG1dPPNOYa";
	public static final String _consumerSecret = "8L8i7YJ9RI3xgnddYdJYrJneHlf1NHmclcVOKHwEGyH687Lyn3";
	public static final String _accessToken = "487610905-khZh5xFM9HWDz0DwH8cRcl2lgK1cL9eIJJrYsc7P";
	public static final String _accessTokenSecret = "VPnRjcarY5iYj6wdDlBFL55zFXf713xRvcKphqEPVfEqZ";
    private static final boolean EXTENDED_TWITTER_MODE = true;
    
    static int contador=0;
	// IMPORTANTE: cambiar ruta
    static File fichero = new File ("/var/home/lab/alum0/03/84/120/eclipse-workspace/TwitterStreaming2/src/cdist/data.txt");
	
	
	
    
    
	public class Handler  {

		public AsynchronousSocketChannel client;
		public ByteBuffer out = ByteBuffer.allocate(1024);
		public ByteBuffer in = ByteBuffer.allocate(1024);

	}

	public static void main(String[] args) throws IOException {
		TwitterProducer tp = new TwitterProducer();

		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.setOAuthConsumerKey(_consumerKey).setOAuthConsumerSecret(_consumerSecret)
				.setOAuthAccessToken(_accessToken).setOAuthAccessTokenSecret(_accessTokenSecret);

		AsynchronousServerSocketChannel server;
		AsynchronousSocketChannel client = null;
		Handler h = tp.new Handler();

		try {
			server = AsynchronousServerSocketChannel.open();
			server.bind(new InetSocketAddress("127.0.0.1", 9999));
			server.accept(h, new CompletionHandler<AsynchronousSocketChannel, Handler>() {

				@Override
				public void completed(AsynchronousSocketChannel result, Handler handler) {
					// prepare for future connections
					if (server.isOpen())
						server.accept(null, this);

					if (result != null && result.isOpen()) {
						handler.client = result;

					}

				}

				@Override
				public void failed(Throwable exc, Handler attachment) {
					// TODO Auto-generated method stub

				}
			});

		} catch (IOException e) {

		}

		TwitterStream twitterStream = new TwitterStreamFactory(configurationBuilder.build()).getInstance();

		twitterStream.addListener(new StatusListener() {

			/* when a new tweet arrives */
			public void onStatus(Status status) {
				contador++;
				// sobreescribir palabra + contador
				FileWriter fw;
				try {
					fw = new FileWriter(fichero, true);
				
				BufferedWriter bw = new BufferedWriter(fw);
				
				if( contador==1) {bw.write(" "+ String.valueOf(contador));}
				else {bw.write(" " + String.valueOf(contador));  }
				
				bw.close();
				fw.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(status.getText());
				if (h != null) {
					if (h.client != null) {
						
						// text almacena el tweet original
						String tweet = status.getText();
						// elminamos los retornos de carro
						String noBreaksTweet = tweet.replace("\n", "").replace("\r", "");
						// procesado de tweets con emojis (dos posibilidades)
						// 1) eliminar los emojis (resultado, tweet sin emojis ni retornos de carro)
						String noBreaksNoEmojisTweet = EmojiParser.removeAllEmojis(noBreaksTweet);
						
						h.out.clear();
						h.out.put(noBreaksNoEmojisTweet.getBytes(StandardCharsets.UTF_8));
						h.out.flip();
						h.client.write(h.out);
					}
				}
				System.out.println("Número de tweets: "+ contador);
			}

			@Override
			public void onException(Exception arg0) {
				System.out.println("Exception on twitter");

			}

			@Override
			public void onDeletionNotice(StatusDeletionNotice arg0) {
				System.out.println("Exception on twitter");

			}

			@Override
			public void onScrubGeo(long arg0, long arg1) {
				System.out.println("onScrubGeo");

			}

			@Override
			public void onStallWarning(StallWarning arg0) {
				System.out.println("EonStallWarning");

			}

			@Override
			public void onTrackLimitationNotice(int arg0) {
				System.out.println("EonTrackLimitationNotice");

			}

			
				
			
		});

		FilterQuery tweetFilterQuery = new FilterQuery(); // See
		
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Ingrese palabra");
		String palabra = scanner.nextLine();
		
		FileWriter fw;
		try {
			fw = new FileWriter(fichero, true);
		
		BufferedWriter bw = new BufferedWriter(fw);
		if( fichero.length()==0) {bw.write(palabra);} //si archivo vacío
		else { bw.write("\n"+palabra);}
		bw.close();
		fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		tweetFilterQuery.track(palabra); // , "Teletubbies"}); // OR on keywords
		
		// ejemplo de localización  (desde USA)
		// tweetFilterQuery.locations(new double[][]{new  double[]{-126.562500,30.448674}, new double[]{-61.171875,44.087585 }});
		// See https://dev.twitter.com/docs/streaming-apis/parameters#locations for
		// proper location doc.
		// Note that not all tweets have location metadata set.
		// ejemplo de idioma  (en inglés)
		/* tweetFilterQuery.language(new String[]{"en"}); */ 
		twitterStream.filter(tweetFilterQuery);
		
	}
}
